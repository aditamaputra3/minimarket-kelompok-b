/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Model.Barang;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class ControllerBarang {
    Scanner sc = new Scanner(System.in);
    ConnectionManajer conMan = new ConnectionManajer();
    Connection con = conMan.LogOn();
    
    public boolean insertBarang(String idBarang, String namaBarang, String jenisBarang, double harga, int stok, String idSupplier){
        String query = "INSERT INTO barang (id_barang, nama_barang, jenis_barang, harga, stok, id_supplier) VALUES"
                + "('"+ idBarang +"','"+ namaBarang +"','"+ jenisBarang +"',"+ harga +","+ stok +",'"+ idSupplier +"')";
        try{
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex){
            System.out.println(ex);
            System.out.println("Input Gagal");
            return false;
        }
    }
    public List<Barang> tampil(){
        List<Barang> listBrg = new ArrayList();
        try{
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM barang");
            while(rs.next()){
                Barang br = new Barang();
                br.setIdBarang(rs.getString("id_barang"));
                br.setNamaBarang(rs.getString("nama_barang"));
                br.setJenisBarang(rs.getString("jenis_barang"));
                br.setHarga(rs.getDouble("harga"));
                br.setStok(rs.getInt("stok"));
                br.setIdSupplier(rs.getString("id_supplier"));
                listBrg.add(br);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listBrg;
    }
    
    public boolean update(String idBarang, String namaBarang, String jenisBarang, double harga, int stok, String idSupplier){
        String query ="UPDATE barang set nama_barang = '"+ namaBarang +"',"
                +"jenis_barang = '"+ jenisBarang +"',harga = "+ harga +", stok = "+ stok +", id_supplier = '"+idSupplier+"'"
                + "WHERE id_barang = '"+ idBarang +"'";
               try {
                   Statement stm = con.createStatement();
                   stm.executeUpdate(query);
                   return true;
                } catch (SQLException ex) {
                    System.out.println(ex);
                    System.out.println("Update Gagal");
                    return false;
                }
    }
    
    public boolean delete(String idBarang){
        String query = "DELETE FROM barang WHERE id_barang ="+ idBarang +"";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            System.out.println("Delete Gagal");
            return false;
        }
    }
    
    public List<Barang> cari(String cari){
        List<Barang> listBrg = new ArrayList();
        try{
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM barang WHERE id_barang LIKE '%" + cari + "%' OR id_supplier LIKE '%" + cari + "'");
            while(rs.next()){
                Barang br = new Barang();
                br.setIdBarang(rs.getString("id_barang"));
                br.setNamaBarang(rs.getString("nama_barang"));
                br.setJenisBarang(rs.getString("jenis_barang"));
                br.setHarga(rs.getDouble("harga"));
                br.setStok(rs.getInt("stok"));
                br.setIdSupplier(rs.getString("id_supplier"));
                listBrg.add(br);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listBrg;
    }
}
