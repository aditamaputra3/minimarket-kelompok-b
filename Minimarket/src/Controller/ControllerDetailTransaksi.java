/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.DetailTransaksi;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author dhea
 */
public class ControllerDetailTransaksi {

    Scanner sc = new Scanner(System.in);
    ConnectionManajer conMan = new ConnectionManajer();
    Connection con = conMan.LogOn();

    public boolean insertDetail(String idTransaksi, String idBarang, int jumlah, double subtotal) {
        String query = "INSERT INTO detail_transaksi (id_transaksi, id_barang, jumlah, subtotal) VALUES"
                + "('" + idTransaksi + "','" + idBarang + "','" + jumlah + "','" + subtotal + "')";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public List<DetailTransaksi> tampil() {
        List<DetailTransaksi> listDet = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM detail_transaksi");
            while (rs.next()) {
                DetailTransaksi dt = new DetailTransaksi();
                dt.setIdTransaksi(rs.getString("id_transaksi"));
                dt.setIdBarang(rs.getString("id_barang"));
                dt.setJumlah(rs.getInt("jumlah"));
                dt.setSubtotal(rs.getDouble("subtotal"));
                listDet.add(dt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return listDet;
    }

    public boolean update(String idTransaksi, String idBarang, int jumlah, double subtotal) {
        String query = "UPDATE detail_transaksi set id_barang = '" + idBarang + "',"
                + "jumlah = '" + jumlah + "', subtotal = '" + subtotal + "'WHERE id_transaksi = " + idTransaksi + "";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public boolean delete(String idTransaksi) {
        String query = "DELETE FROM detail_transaksi WHERE id_transaksi = '" + idTransaksi + "'";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public List<DetailTransaksi> cari(String cari) {
        List<DetailTransaksi> listDet = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM detail_transaksi WHERE id_transaksi LIKE '%" + cari + "%' OR id_barang LIKE '%" + cari + "'");
            while (rs.next()) {
                DetailTransaksi dt = new DetailTransaksi();
                dt.setIdTransaksi(rs.getString("id_transaksi"));
                dt.setIdBarang(rs.getString("id_barang"));
                dt.setJumlah(rs.getInt("jumlah"));
                dt.setSubtotal(rs.getDouble("subtotal"));
                listDet.add(dt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return listDet;
    }
}
