/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Supplier;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author dhea
 */
public class ControllerSupplier {
    Scanner sc = new Scanner(System.in);
    ConnectionManajer conMan = new ConnectionManajer();
    Connection con = conMan.LogOn();
    
    public boolean insertSupp (String idSupplier, String namaSupplier) {
        String query = "INSERT INTO supplier values('" + idSupplier + "','"
                + namaSupplier + "')";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("Input Gagal");
            return false;
        }
    }
    
    public List<Supplier> tampilSupp() {
        List<Supplier> listSupp = new ArrayList<Supplier>();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM supplier");
            while(rs.next()) {
                Supplier spl = new Supplier();
                spl.setIdSupplier(rs.getString("id_supplier"));
                spl.setNamaSupplier(rs.getString("nama_supplier"));
                listSupp.add(spl);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return listSupp;
    }
    
    public boolean updateSupp(String idSupplier, String namaSupplier){
        String query = "UPDATE suppplier SET nama_supplier ='"+ namaSupplier
                 +"' WHERE id_sustomers='"+idSupplier+"'";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("Update Gagal");
            return false;
        }
    }
    
    public boolean deleteSupp(String idSupplier){
        String query = "DELETE FROM supplier WHERE id_supplier='"+idSupplier+"'";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("Delete Gagal");
            return false;
        }
    }
    
    public List<Supplier> cariSupp(String cari) { 
        List<Supplier> listSupp = new ArrayList<Supplier>();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM supplier WHERE id_supplier LIKE '%"+ cari +"%' OR nama_supplier LIKE '%" + cari + "'");
            while(rs.next()) {
                Supplier spl = new Supplier();
                spl.setIdSupplier(rs.getString("id_supplier"));
                spl.setNamaSupplier(rs.getString("nama_supplier"));
                listSupp.add(spl);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return listSupp;
    }
}
