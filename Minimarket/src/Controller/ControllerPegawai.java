/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Pegawai;
import Model.Supplier;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adi
 */
public class ControllerPegawai {

    ConnectionManajer conMan = new ConnectionManajer();
    Connection con = conMan.LogOn();

    public boolean tambahPegawai(String idPegawai, String namaPegawai, String jabatan, String alamat, String email, String password) {
        String query = "INSERT INTO pegawai values ('" + idPegawai + "','" + namaPegawai + "','" + jabatan + "','" + alamat + "','"
                + email + "','" + password + "')";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            System.out.println(ex);
            return false;
        }
    }

    public List<Pegawai> tampilPegawai() {
        List<Pegawai> listPgw = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM pegawai");
            while (rs.next()) {
                Pegawai pgw = new Pegawai();

                pgw.setIdPegawai(rs.getString("id_pegawai"));
                pgw.setNamaPegawai(rs.getString("nama_pegawai"));
                pgw.setJabatan(rs.getString("jabatan"));
                pgw.setAlamat(rs.getString("alamat"));
                pgw.setEmail(rs.getString("email"));
                pgw.setPassword(rs.getString("password"));

                listPgw.add(pgw);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return listPgw;
    }

    public boolean updatePegawai(String idPegawai, String namaPegawai, String jabatan, String alamat, String email, String password) {
        String query = "UPDATE pegawai set id_pegawai = '" + idPegawai + "', nama_pegawai = '" + namaPegawai + "', jabatan = '" + jabatan + "', alamat = '" + alamat + "',"
                + "email = '" + email + "', password = '" + password + "'  WHERE id_pegawai = '" + idPegawai + "'";

        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            //mengetahui kesalahan yang terdapat pada query
            System.out.println(ex);
            return false;
        }
    }

    public boolean deletePegawai(String idPegawai) {
        String query = "DELETE FROM pegawai WHERE id_pegawai ='" + idPegawai + "'";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (SQLException ex) {
            //mengetahui kesalahan yang terdapat pada query
            System.out.println(ex);
            return false;
        }
    }

    public List<Pegawai> cariPegawai(String cari) {
        List<Pegawai> listPgw = new ArrayList<Pegawai>();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM pegawai WHERE id_pegawai LIKE '%" + cari + "%' OR nama_pegawai LIKE '%" + cari + "'");
            while (rs.next()) {
                Pegawai pgw = new Pegawai();
                pgw.setIdPegawai(rs.getString("id_pegawai"));
                pgw.setNamaPegawai(rs.getString("nama_pegawai"));
                pgw.setJabatan(rs.getString("jabatan"));
                pgw.setAlamat(rs.getString("alamat"));
                pgw.setEmail(rs.getString("email"));
                pgw.setPassword(rs.getString("password"));
                listPgw.add(pgw);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return listPgw;
    }
}
