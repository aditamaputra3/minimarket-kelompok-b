/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Pembeli;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author jaiz
 */
public class ControllerPembeli {

    Scanner sc = new Scanner(System.in);
    ConnectionManajer conMan = new ConnectionManajer();
    Connection con = conMan.LogOn();

    public void insertPembeli(String idPembeli, String namaPembeli, String noTelp, String alamat) {
        String query = "INSERT  INTO  pembeli  (id_pembeli,  nama_pembeli,  " + "no_telp, alamat)  values  " + "('" + idPembeli + "',"
                + "'" + namaPembeli + "','" + noTelp + "','" + alamat + "')";
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    public void update(String idPembeli, String namaPembeli, String noTelp, String alamat) {
        int result = 0;
        String query = "UPDATE pembeli set nama_pembeli = '" + namaPembeli + "', " + "no_telp = '" + noTelp + "', " + "alamat = '" + alamat + "' "
                + "WHERE id_pembeli = '"+idPembeli+"'";
        try {
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    public void delete(String idPembeli) {
        int result = 0;
        String query = "DELETE FROM pembeli WHERE id_pembeli = '"+idPembeli+"'";
        try {
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    public List<Pembeli> tampil() {
        List<Pembeli> listPem = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM pembeli");
            while (rs.next()) {
                Pembeli pem = new Pembeli();
                pem.setIdPembeli(rs.getString("id_pembeli"));
                pem.setNamaPembeli(rs.getString("nama_pembeli"));
                pem.setNoTelp(rs.getString("no_telp"));
                pem.setAlamat(rs.getString("alamat"));
                listPem.add(pem);

            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return listPem;
    }
    
    public List<Pembeli> caripembeli(String cari) { 
        List<Pembeli> listPembeli = new ArrayList<Pembeli>();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM pembeli WHERE id_pembeli LIKE '%"+ cari +"%' OR nama_pembeli LIKE '%" + cari + "'");
            while(rs.next()) {
                Pembeli pem = new Pembeli();
                pem.setIdPembeli(rs.getString("id_pembeli"));
                pem.setNamaPembeli(rs.getString("nama_pembeli"));
                pem.setNoTelp(rs.getString("no_telp"));
                pem.setAlamat(rs.getString("alamat"));
                listPembeli.add(pem);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return listPembeli;
    }

}
