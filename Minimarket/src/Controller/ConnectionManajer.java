/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author adi
 */
public class ConnectionManajer {

    public static Connection con;
    public static Statement stat;
    String Driver = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://localhost/uas_minimarket";
    String user = "root";
    String pass = "";

    public Connection LogOn() {
        try {
            Class.forName(Driver).newInstance();
            con = DriverManager.getConnection(url, user, pass);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return con;
    }

    public void LogOff() {
        try {
            con.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
