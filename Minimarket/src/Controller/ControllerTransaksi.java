/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import Model.Transaksi;

/**
 *
 * @author Kirana
 */
public class ControllerTransaksi {

    ConnectionManajer conMan = new ConnectionManajer();
    Connection con = conMan.LogOn();

    public boolean addTransaksi(String idTransaksi, String idPegawai, String idPembeli, 
            double subtotal, double diskon, double total, String tanggal) {
        String query = "INSERT INTO transaksi values('"
                + idTransaksi + "','" + idPegawai + "','" + idPembeli + "'," 
                + subtotal + "," + diskon + "," + total + ",'" + tanggal + "')";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("Input Gagal");
            return false;
        }
    }

    public List<Transaksi> tampil() {
        List<Transaksi> listTransaksi = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM transaksi");
            while (rs.next()) {
                Transaksi tr = new Transaksi();
                tr.setIdTransaksi(rs.getString("id_transaksi"));
                tr.setIdPegawai(rs.getString("id_pegawai"));
                tr.setIdPembeli(rs.getString("id_pembeli"));
                tr.setSubtotal(Double.parseDouble(rs.getString("subtotal")));
                tr.setDiskon(Double.parseDouble(rs.getString("diskon")));
                tr.setTotal(Double.parseDouble(rs.getString("total")));
                tr.setTanggal(rs.getString("tanggal"));

                listTransaksi.add(tr);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return listTransaksi;
    }

    public boolean deleteTransaksi(String idTransaksi){
        String query = "DELETE FROM transaksi WHERE id_transaksi='"+idTransaksi+"'";
        try {
            Statement stm = con.createStatement();
            stm.executeUpdate(query);
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("Input Gagal");
            return false;
        }
    }
    
    public List<Transaksi> cariTransaksi(String cari) {
        
        List<Transaksi> listTransaksi= new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM transaksi WHERE idTransaksi LIKE '%");
            while(rs.next()) {
                Transaksi tr= new Transaksi();
                tr.setIdTransaksi(rs.getString("id_transaksi"));
                tr.setIdPegawai(rs.getString("id_pegawai"));
                tr.setIdPembeli(rs.getString("id_pembeli"));
                tr.setSubtotal(Double.parseDouble(rs.getString("subtotal")));
                tr.setDiskon(Double.parseDouble(rs.getString("diskon")));
                tr.setTotal(Double.parseDouble(rs.getString("total")));
                tr.setTanggal(rs.getString("tanggal"));
                
                listTransaksi.add(tr);
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return listTransaksi;
    }
}
