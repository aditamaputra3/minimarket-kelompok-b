/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author dhea
 */
public class Supplier {

    private String idSupplier;
    private String namaSupplier;

    public Supplier() {
    }

    public Supplier(String idSupplier, String namaSupplier) {
        this.idSupplier = idSupplier;
        this.namaSupplier = namaSupplier;
    }

    public String getIdSupplier() {
        return idSupplier;
    }

    public void setIdSupplier(String idSupplier) {
        this.idSupplier = idSupplier;
    }

    public String getNamaSupplier() {
        return namaSupplier;
    }

    public void setNamaSupplier(String namaSupplier) {
        this.namaSupplier = namaSupplier;
    }

}
