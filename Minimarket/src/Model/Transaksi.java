/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Kirana
 */
public class Transaksi {
    private String idTransaksi;
    private String idPegawai;
    private String idPembeli;
    private double subtotal;
    private double diskon;
    private double total;
    private String tanggal;

    public Transaksi() {
    }

    public Transaksi(String idTransaksi, String idPegawai, String idPembeli, double subtotal, double diskon, double total, String tanggal) {
        this.idTransaksi = idTransaksi;
        this.idPegawai = idPegawai;
        this.idPembeli = idPembeli;
        this.subtotal = subtotal;
        this.diskon = diskon;
        this.total = total;
        this.tanggal = tanggal;
    }

    public String getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(String idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public String getIdPegawai() {
        return idPegawai;
    }

    public void setIdPegawai(String idPegawai) {
        this.idPegawai = idPegawai;
    }

    public String getIdPembeli() {
        return idPembeli;
    }

    public void setIdPembeli(String idPembeli) {
        this.idPembeli = idPembeli;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiskon() {
        return diskon;
    }

    public void setDiskon(double diskon) {
        this.diskon = diskon;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
    
    
}
