package Model;

/**
 *
 * @author jaiz
 */

public class Pembeli {

    private String idPembeli;
    private String namaPembeli;
    private String noTelp;
    private String alamat;

    public Pembeli() {

    }

    public Pembeli(String idPembeli, String namaPembeli, String noTelp, String alamat) {
        this.idPembeli = idPembeli;
        this.namaPembeli = namaPembeli;
        this.noTelp = noTelp;
        this.alamat = alamat;
    }

    public String getIdPembeli() {
        return idPembeli;
    }

    public void setIdPembeli(String idPembeli) {
        this.idPembeli = idPembeli;
    }

    public String getNamaPembeli() {
        return namaPembeli;
    }

    public void setNamaPembeli(String namaPembeli) {
        this.namaPembeli = namaPembeli;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

}
