/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author adi
 */
public class Pegawai {

    private String idPegawai;
    private String namaPegawai;
    private String jabatan;
    private String alamat;
    private String email;
    private String password;

    public Pegawai() {

    }

    public Pegawai(String idPegawai, String namaPegawai, String jabatan, String alamat, String email, String password) {
        this.idPegawai = idPegawai;
        this.namaPegawai = namaPegawai;
        this.jabatan = jabatan;
        this.alamat = alamat;
        this.email = email;
        this.password = password;
    }

    public String getIdPegawai() {
        return idPegawai;
    }

    public void setIdPegawai(String idPegawai) {
        this.idPegawai = idPegawai;
    }

    public String getNamaPegawai() {
        return namaPegawai;
    }

    public void setNamaPegawai(String namaPegawai) {
        this.namaPegawai = namaPegawai;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
